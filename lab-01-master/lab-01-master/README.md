# Jenkins Infrastructure
Lab 01: Setting up the required infrastructure with Docker

---

## Preparation

 - Ask the instructor for the IP and credentials of the following servers:
	- Infrastructure
	- Test
	- Production
 - Keep in mind that case sensitive matters!

## Tasks

 - Setup Jenkins master
 - Add a slave on the infrastructure server for CI pipelines
 - Add a slave on the Test environment server for CD pipelines
 - Add a slave on the Production environment server for CD pipelines

## Architecture

<img alt="Image 3" src="Images/lab01-infra.png"  width="50%" height="50%">

## Instructions

 - Access to your infrastructure server
```
ssh "<your-infrastructure-server-ip>"
```

 - Define your server ip as an env var:
```
export SERVER_IP="<your-infrastructure-server-ip>"

echo $SERVER_IP
```
 
 - Use docker to set up the Jenkins environment :

```
docker run -p 8080:8080 \
-p 50000:50000 \
-v jenkins_home:/var/jenkins_home \
--name jenkins \
--memory="3g" \
--restart=always \
--hostname="${SERVER_IP}" \
-d \
jenkinsci/blueocean:1.24.1 

```


### Jenkins Configuration (Infrastructure Server)

 - Browse from the **browser** to the jenkins portal:

```
http://<Infrastructure-Server-Ip>:8080
```

 - Unlock jenkins using the administrator password, use the command below to retrieve it:

```
$ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

![Image 1](Images/lab01-jenkins-01.png)

 - Select "Install suggested plugins" and wait for the plugins to being installed: 

![Image 2](Images/lab01-jenkins-02.png)

 - **If you get installation failures, click on continue (most of the plugins are not required)**

 - You will be asked for credentials, set the details below:

```
Username: sela
Password: sela
FullName: sela
Email: sela@sela.com
```

<img alt="Image 3" src="Images/lab01-jenkins-03.png"  width="75%" height="75%">

 - As Jenkins URL set:

 ```
 http://<infrastructure-server-ip>:8080
 ```
  
 - Note: You can update the user details in "Manage Jenkins"/"Manage Users"/"settings"

<img alt="Image 4" src="Images/lab01-jenkins-04.png"  width="75%" height="75%">
 
 - Once finished, click on "Restart" to restart jenkins and apply the configurations

 - Check the status of the "Restart" operation by cliking on "F5" key.


### Jenkins Slave Configuration (Infrastructure Server)

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure Credentials via the **jenkins portal**:
 

```
"Manage Jenkins" → "Manage Credentials" → "Jenkins" → "Global credentials (unrestricted) → Add Credentials"
```

![Image 6](Images/lab01-cred-01.png)

 - Enter the following details:
 

```
    Kind        - username with password		
 	Scope       - Global...
 	Username    - sela
 	Password    - sela	
 	ID          - sela_cred
 	Description - sela
```

 ![Image 7](Images/lab01-cred-02.png)
 
 - Configure the server as a jenkins slave, start creating a new node in the **jenkins portal**:

```
"Manage Jenkins" → "Manage Nodes" → "New Node"
Set "Slave" as name and select "Permanent Agent"
```
 
![Image 5](Images/lab01-slave-01.png)
 
 - Configure the slave with the details below:

```
number of executors: 5
Remote root directory: /home/jenkins
Labels: Slave
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <infrastructure-server-ip>
Credentials: Sela/*****
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")

 - If you get some issues at this point, click on the node name and inspect the logs

 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```


### Configure Test Environment (Jenkins Slave)

 - Connect to the test environment server:

```
$ ssh sela@<test-environment-server-ip>
```

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure the server as a jenkins slave, start creating a new node in the **jenkins portal**:

```
"Manage Jenkins" → "Manage Nodes" → "New Node"
Set "Test" as name and select "Permanent Agent"
```
 
 - Configure the slave with the details below:

```
number of executors: 1
Remote root directory: /home/jenkins
Labels: Test
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <test-environment-server-ip>
Credentials: Sela/*****
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")
 
 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```


### Configure Production Environment (Jenkins Slave)

 - Connect to the test environment server:

```
$ ssh sela@<production-environment-server-ip>
```

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure the server as a jenkins slave, start creating a new node in the **jenkins portal**:

```
"Manage Jenkins" → "Manage Nodes" → "New Node"
Set "Production" as name and select "Permanent Agent"
```
 
 - Configure the slave with the details below:

```
number of executors: 1
Remote root directory: /home/jenkins
Labels: Production
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <production-environment-server-ip>
Credentials: Sela/*****
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")
 
 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```
